int solution(int X, int Y, int D) {

    Y -= X;
    
    X = Y % D;
    Y /= D;
    if (X != 0)
        return ++Y;
        
    return Y;
}