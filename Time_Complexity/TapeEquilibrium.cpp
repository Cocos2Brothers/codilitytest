// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {
    
    
    int sumLeft = A.at(0),
        sumRight = 0,
        //minDiffrence = 0;
        diffrence = 0;
        

    
    for(auto it = (A.begin() + 1); it < A.end(); ++it)
    {
        sumRight += *it;    
    }
    
    /*if (A.size() == 2)
    {
        diffrence = abs(sumLeft - sumRight);
        return diffrence;
    }
    */
    
    diffrence = abs(sumLeft - sumRight);
    
    for(auto it = (A.begin() + 1); it < (A.end() - 1); ++it)  // ważne żeby był A.end()-1 to rozwiązuje problem przy tylko 2 elementach
    {
        sumLeft += *it;
        sumRight -= *it;
        
        if (diffrence > abs(sumLeft - sumRight))
            diffrence = abs(sumLeft - sumRight);
    }
    
    
    return diffrence;
}