int solution(vector<int> &A) {
    
    int numberOfElem = A.size() + 1 ;
    
    if ( 1 == numberOfElem )  return 1;

    long long sumOfArithmetic =  ((1 + (long long )numberOfElem ) * (long long )numberOfElem )/ (long long )2;
   
    long long sumOfData = 0; 
    for ( auto el : A) 
        sumOfData += el; 
        
    //std::cout << sumOfArithmetic << std::endl;
    // std::cout << sumOfData << std::endl;

    
    return sumOfArithmetic - sumOfData;
    
    
}
