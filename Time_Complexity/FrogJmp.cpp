
//https://codility.com/demo/results/trainingUEBJRP-ZB6/
// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;


int solution(int X, int Y, int D) {

    
    if ( 1 == D )
        return Y-X;
    
	int diffJumps = (Y - X); 
	
    if ( diffJumps % D  == 0 )
    {    
        return diffJumps/D;
    }
    else 
    {
        return diffJumps/D +1; 
    }

}

