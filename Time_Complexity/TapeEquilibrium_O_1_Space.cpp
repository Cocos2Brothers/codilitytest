int solution(vector<int> &A) {
    
    int sumOfVec = 0; 
    for (auto it = A.begin(); it != A.end(); ++it) 
    {
        sumOfVec += *it;
    }
    
    
    int minDiff = 100000*1000+1; // less than 2^31; 
    int leftSide = 0;
    int rightSide = sumOfVec; 
    
    for (size_t i = 0; i < A.size() - 1; ++i) 
    {
        leftSide += A.at(i);
        rightSide -= A.at(i);
        
        int diff = abs( leftSide - rightSide  );
        if ( minDiff > diff )
        {
            minDiff = diff; 
            if ( 0 == minDiff )
            {
                break; 
            }
        }
            
    }
    
    return minDiff; 
}





