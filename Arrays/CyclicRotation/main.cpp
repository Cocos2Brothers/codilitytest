/* 
 * File:   main.cpp
 * Author: grdu
 *
 * Created on July 6, 2016, 2:17 PM
 */

#include <cstdlib>
#include <vector>

using namespace std;

std::vector < int > solution (std::vector <int> &A, int K) ;

/*
 * 
 */
int main(int argc, char** argv)
{

    return 0;
}

//https://codility.com/demo/results/trainingHD3WY9-39G/


vector<int> solution(vector<int> &A, int K) {
    // write your code in C++11 (g++ 4.8.2)
    
    int size = A.size();
    
    std::vector<int> res ( size);
    
    for ( int i = 0 ; i < size ; ++i ) 
    {
        res.at( (i + K  )%size ) = A.at( i );
        
 
    }
    
    return res;
    
}

//[1,2,3,4,5,6,7,8,9], 1 

