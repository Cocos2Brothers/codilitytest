

// you can use includes, for example:
#include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {
    std::sort( A.begin(), A.end() );
    
    auto it = A.begin();
    for (; ; it +=2) 
    {
        if (*it != *(it+1)) break;
    }
    
    return *it; 
}

