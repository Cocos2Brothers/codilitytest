// you can use includes, for example:
#include <algorithm>
#include <limits>       // std::numeric_limits
// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {

    unsigned int max_ending = 0, max_slice =0; 
    int minElem =  std::numeric_limits<int>::max() ; 
    size_t beginSlice = 1, endSliec = A.size();
    
    for (size_t i = 1 ; i < A.size() - 1 ; ++i) 
    {
        max_ending = std::max(0u, max_ending + A.at(i) );
        
        if (max_ending == 0 )
        {
            beginSlice = i;
        }
        if  (max_slice < max_ending)
        {
            endSliec = i;
        }
        
        
        max_slice = std::max(max_slice, max_ending );

    }
    
    std::cout << beginSlice << " " << endSliec << std::endl; 
    
    for (size_t i = beginSlice ; i <= endSliec   ; ++i) 
    {
        if ( minElem > A.at(i) )
        {
            minElem = A.at(i);
            std::cout << "new min " << minElem << std::endl;

        }
    }
    
    std::cout << minElem << std::endl;
    
    if (minElem == std::numeric_limits<int>::max() ) minElem = 0;
    
    

    return max_slice - minElem;

}
//For example, for the input [5, 5, 5] the solution returned a wrong answer (got -95 expected 0). 
//For example, for the input [5, 17, 0, 3] the solution returned a wrong answer (got 12 expected 17). 
