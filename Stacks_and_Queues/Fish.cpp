

#include  <stack>

int solution(vector<int> &A, vector<int> &B) {
/*
    if stack empty then push AB continue;
    
    if topB == B(i) or topB is 0 then push AB
    
    if topB !=B(i) or topB is not 0 then:
        topA > A(i) then continue ( A(i) eaten) 
        topA < A(i) then popAB, --i ( topAB eaten);

*/
        
    std::stack <int> stackA, stackB;
    size_t i = 0;

    for (; i < A.size(); ++i ) 
    {

        if (stackA.empty())
        {
            stackA.push( A.at(i) ); 
            stackB.push( B.at(i) ); 
            continue;
        }

                
        if (stackB.top() == B.at(i) || stackB.top() == 0) 
        {
            stackA.push( A.at(i) ); 
            stackB.push( B.at(i) ); 
            continue;
        }
        
        if (stackB.top() != B.at(i) && stackB.top() != 0 ) 
        {
            if ( stackA.top() > A.at(i) ) 
            {
                continue;
            }
            if ( stackB.top() < A.at(i) )
            {
                stackB.pop();
                stackA.pop(); 
                --i; 
                continue;
            }


        }
        
    }
    return stackA.size();
}


