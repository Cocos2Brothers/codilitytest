

#include <stack> 

int solution(string &S) {
    
    if ( S.empty() ) return 1;
    
    std::stack <char> bracket;
    
    for (char a : S ) 
    {
       // std::cout << a << std::endl;
        
        if ( '(' == a || '{' == a  || '[' == a )
        {
            bracket.push(a); 
        }
        else 
        {
            if ( bracket.empty() )
            {
                return 0;
            }
            char top = bracket.top(); 
            
        //    std::cout << " top " << bracket.top() << std::endl;
            
            if ( ']' == a && '[' == top ) 
            {
                bracket.pop();
            }
            else if ( ')' == a && '(' == top ) 
            {
                bracket.pop();
            }
            else if ( '}' == a && '{' == top ) 
            {
                bracket.pop();
            }
            else 
            {
                return 0;
            }
            
        }
            
        
    }
    
   // std::cout << bracket.size() << std::endl;
    return bracket.size() == 0 ? 1: 0; 
}


