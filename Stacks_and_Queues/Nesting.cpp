int solution(string &S) 
{
    if ( S.empty() ) return 1; 
    
   // if ( ')' == S.at(0) ) return 0;
    
    int openBrack = 0; 
    
    for ( char & a : S ) 
    {
        if (openBrack < 0 ) return 0;
        
        if ( '(' == a ) ++openBrack;
        else if ( ')' == a ) --openBrack;    
        else throw 0; 
    }
    if ( 0 == openBrack) 
        return 1;
    else 
        return 0; 
}
