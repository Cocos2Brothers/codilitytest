

// you can use includes, for example:
#include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {

    int size = A.size();
    if (size<2) return 0; 
    
    std::sort(A.begin(), A.end()); 
    
    for (int i = 0; i < size -2; ++i)
    {
        if ((long)A.at(i) + (long)A.at(i+1) > (long)A.at(i+2))
        {
            return 1; 
        }
    }

    return 0; 

}


