// you can use includes, for example:
#include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {

    if (A.empty()) return 0; 
    
    std::sort (A.begin(), A.end()); 
    
    size_t i = 1;
    int counter = 1; 
    for ( ; i < A.size() ; ++i)
    {
        if (  A.at(i-1) != A.at(i) )
        {
            ++counter;
        }
    }
    
    
    return counter; 

}

