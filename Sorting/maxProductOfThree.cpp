#include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {

    std::sort (A.begin(), A.end());

    size_t s = A.size();
    int m  = A.at(s-1)*A.at(s-2)*A.at(s-3);
    int m2 = A.at(0)*A.at(1)*A.at(s-1); 
    
    return std::max (m , m2);
}	
