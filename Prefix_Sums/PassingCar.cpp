
int solution(vector<int> &A) {
    
    
    int EXCEED = 1000000000;
    
    int sumOfWest = 0; 
    int passingCar = 0;
    
    for ( auto rit = A.crbegin(); rit != A.crend(); ++rit)
    {
        if ( 1 == *rit ) 
        {
            ++sumOfWest;
        }
        else 
        {
            passingCar += sumOfWest;
            
            if ( EXCEED < passingCar ) return -1; 
        }
        
        
    }
    
   // std::cout << sumOfWest << std::endl;
    

   // std::cout << passingCar << std::endl;

    return passingCar;
}
  


//1000000000
//2147483648
