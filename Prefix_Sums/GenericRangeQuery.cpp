// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

vector<int> solution(string &S, vector<int> &P, vector<int> &Q) {
   
   std::vector <int> nucleotideA (S.length()); 
   std::vector <int> nucleotideC (S.length()); 
   std::vector <int> nucleotideG (S.length()); 
   std::vector <int> nucleotideT (S.length()); 
   
   int sumA = 0, sumC = 0, sumG = 0, sumT = 0; 
   
   for (size_t i = 0 ; i < S.length() ; ++i)
   {
       switch (S.at(i))
       {
            case 'A':
                sumA++;    
                break;
            case 'C':
                sumC++;    
                break;
            case 'G':
                sumG++;    
                break;            
            case 'T':
                sumT++;    
                break;
       }
       nucleotideA.at(i) = sumA;
       nucleotideC.at(i) = sumC;
       nucleotideG.at(i) = sumG;
       nucleotideT.at(i) = sumT;
   }
   
 //  for ( int a : nucleotideT)
 //  {
  //     std::cout << a << " "; 
  // }
   
 // std::cout << std::endl;
   
   std::vector <int> res(P.size(),5); 
   
   for (size_t i = 0; i < P.size(); ++i)
   {
        int left = P.at(i) -1 ;
        int right = Q.at(i); 
        
        
    //  std::cout << left << "   " << right <<std::endl;
        
        
        if ( 0 > left) 
        {
            //std::cout << left << "   ffsdfs" << right <<std::endl;

            left = 0; 
            if ( nucleotideA.at(left) == 1)   res.at(i) = 1 ;
            if ( nucleotideC.at(left) == 1)   res.at(i) = 2 ;
            if ( nucleotideG.at(left) == 1)   res.at(i) = 3 ;
            if ( nucleotideT.at(left) == 1)   res.at(i) = 4 ;

        }
       
/*        std::cout << nucleotideA.at(left) << " " << nucleotideA.at(right) <<std::endl;
        std::cout << nucleotideC.at(left) << " " << nucleotideC.at(right) <<std::endl;
        std::cout << nucleotideG.at(left) << " " << nucleotideG.at(right) <<std::endl;
        std::cout << nucleotideT.at(left) << " " << nucleotideT.at(right) <<std::endl;
*/

       
        if ( nucleotideA.at(left) < nucleotideA.at(right)  )
        {
            res.at(i) = 1; 
            continue;
        }
        
        if ( nucleotideC.at(left) < nucleotideC.at(right) && res.at(i) > 2)
        {
            res.at(i) = 2; 
            continue;
        }
       
        if ( nucleotideG.at(left) < nucleotideG.at(right) && res.at(i) > 3 )
        {
            res.at(i) = 3; 
            continue;
        }
       
        if ( nucleotideT.at(left) < nucleotideT.at(right)  && res.at(i) > 3 )
        {
            res.at(i) = 4; 
            continue;
        }
   }
   
   
   
   return res; 
}
