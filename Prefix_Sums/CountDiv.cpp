int solution(int A, int B, int K) 
{
    // write your code in C++11 (g++ 4.8.2)
    
//     2,000,000,000
//2^31 2,147,483,648
    
    int bk = B/K;
    int ak = A/K;
    
    
    
    int result = bk - ak; 
    
    
    if ( 0 == A%K ) ++result;
    
   // std::cout << bk << std::endl;
//    std::cout << ak << std::endl;
    
    
    return result;
}

// A = 0, B = MAXINT, K in {1,MAXINT} 
//[0, 0, 11]
//[0, 10 ,10]
//[0, 10 ,1]
//[10, 10 ,1]
//[10, 10 ,1]
//[10, 10 ,2]
