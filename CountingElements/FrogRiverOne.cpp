#include <vector> 

int solution(int X, vector<int> &A) 
{
    
    std::vector< bool > frogRoad ( X + 1 ); 
    
    int neededLeaves = X  ; 
    
    size_t i = 0; 
    for ( ; i < A.size() ; ++i ) 
    {
        if ( frogRoad.at( A.at(i) ) == true)
        {
            continue;
        }
        else 
        {
            frogRoad.at( A.at(i) ) = true;
            --neededLeaves;
            if ( 0 == neededLeaves ) 
            {
                return i; 
            }
        }
    }
    return -1; 	
}
 
