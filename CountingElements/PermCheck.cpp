int solution( vector<int> &A) 
{
    
    size_t sizeOfA = A.size(); 
    
    std::vector<bool> permCheck ( sizeOfA + 1 ) ; 
    
    size_t counter = sizeOfA; 
    
    for (size_t i = 0  ; i < sizeOfA ; ++i )
    {
      //  std::cout <<  A.at(i)  <<  " " << i << std::endl;
        if ( A.at(i) > (int)sizeOfA )
        {
            return 0; 
        }
        
        if ( permCheck.at( A.at(i) ) == true )
        {
            return 0; 
        }
        else 
        {
            permCheck.at( A.at(i) ) = true;
            --counter;

            if ( counter == 0 && i == (sizeOfA-1) ) return 1; 
        }
    }

    return 0; 
}
