inline void newMax (int & val, int & maxVal) ;

vector<int> solution(int N, vector<int> &A) {
    
    
    std::vector <int> result (N);
    
    int lastMax = 0;
    int maxVal = 0; 
    for (int val : A )
    {
        if (val == N+1 )
        {
               lastMax = maxVal;
        }
        else 
        {
            if ( result.at( val-1 ) < lastMax )
            {
                result.at( val-1 ) = lastMax+1;
                newMax( result.at( val-1 ), maxVal );

            }
            else 
            {
                result.at( val-1 ) += 1; 
                newMax( result.at( val-1 ), maxVal );
            }
        }
        
    }
    
    for ( int & val: result ) 
    {
        if ( val < lastMax)
        {
            val = lastMax; 
        }
    }
    
    
    return result; 
}

inline void newMax (int & val, int &maxVal) 
{
    if ( val  > maxVal )
    {
        maxVal = val;
    }
    
}
