
int solution(vector<int> &A) {
    
    int size = A.size()+1;
    std::vector<bool> positiveBit (size);
    
    for (auto it = A.begin(); it !=A.end(); ++it ) 
    {
        if ( (*it < size) && (*it > 0) ) 
        {
        
            positiveBit.at(*it) = true; 
       //     std::cout << *it << std::endl;
        }
    }
    
    
    
  //  std::cout << "size "<< positiveBit.size() << std::endl;
    
    
    int i = 1;

    for (; i<size; ++i)
    {
     //   std::cout << positiveBit.at(i) << std::endl;
        if (positiveBit.at(i) == false ) 
            return i;
    }
     
     return i;
}
