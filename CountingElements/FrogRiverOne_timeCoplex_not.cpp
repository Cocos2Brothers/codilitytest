	
#include <unordered_set>

int solution(int X, vector<int> &A) 
{

    std::unordered_set <int> fallenLeaves; 
    
    size_t i = 0; 
    for ( ; i < A.size(); ++i) 
    {
        fallenLeaves.insert ( A.at(i) ); 
    //    std::cout << i << "  " << fallenLeaves.size() << std::endl;

        if (fallenLeaves.size() == (size_t) X   ) break;
    }
    if ( i == A.size() ) return -1; 

    return i;
}
