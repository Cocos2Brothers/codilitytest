int solution(vector<int> &A) {
    
    int value = 0; 
    int size = 0; 
    
    
    for ( auto & a : A ) 
    {
        if ( 0 == size ) 
        {
            ++size;
            value = a; 
        }
        else 
        {
            if  ( value == a ) 
            {
                ++size;
            }
            else 
            {
                --size; 
            }
        }

    }
    
    int candidate = -1;
    if (size > 0 ) 
    {
        candidate = value;  
    }
    
   // std::cout << " candidate " << candidate << std::endl;
    
    size_t count = 0 ;
    for (auto & i : A) 
    {
        if ( i == candidate )
        {
            ++count;
        }
    }
    
    
    if ( count <= ( A.size() / 2u ) )
    {
        return 0; 
    }
    
    
    size_t foundCandidate = 0; 
    size_t sizeA = A.size(); 
    int equiLeaderCounter = 0; 
    
  //  std::cout << sizeA << std::endl;
    
    for (size_t i = 0; i < sizeA; ++i)
    {
        if ( candidate == A.at(i) )
        {
            ++foundCandidate;
        }
        size_t plusOne = i + 1; 
      //  std::cout << "count " << count << " foundCandidate " << foundCandidate << " i " << i << std::endl;
      //  std::cout << ((count - foundCandidate) > ((sizeA - plusOne)/2u))   <<  "\t " << (foundCandidate > ( plusOne /2u)) << std::endl;
      //  std::cout << (count - foundCandidate) << " " <<  ((sizeA - plusOne )/2u) << "      " << foundCandidate  << " " << (plusOne/2u) << std::endl;
        
        if ( (count - foundCandidate) > ((sizeA - plusOne)/2u) && foundCandidate > (plusOne/2u) )
        {
            ++equiLeaderCounter; 
        }        
    }
    

    
    return  equiLeaderCounter; 
    
}