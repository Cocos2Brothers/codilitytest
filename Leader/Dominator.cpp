

int solution(vector<int> &table) {
    if ( 0 == table.size() ) return -1; 
    
    int candidate = 0; 
    int counterCandidate = 0; 
    size_t firstOccurence = 0; 
    //for (int & val : A ) 
    for (size_t i = 0 ; i < table.size() ; ++i ) 
    {
        int val = table.at(i); 
        
        if ( 0 == counterCandidate ) 
        {
            candidate = val; 
            ++counterCandidate;
            firstOccurence = i ; 
        }
        else  
        {
            if ( val == candidate ) 
            {
                ++counterCandidate;
            }
            else 
            {
                --counterCandidate;
            }
                
        }
    }
    
    if ( counterCandidate == 0 ) return -1; 
    
    size_t count = 0 ;
    for (auto & i : table) 
    {
        if ( i == candidate )
        {
            ++count;
        }
    }
    
    
    if ( count*2u <=  table.size() )
    {
        return -1; 
    }
    
    return firstOccurence; 

}
// [ 4,4,4,4,4, 4,4,4,4,4,   1,1,1,1,1, 1,1,1,1,1]

